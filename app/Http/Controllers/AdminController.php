<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index(){
        $data = DB::table('users')->select()->where('role', '0')->get();
        return view('admin')->with(compact('data'));
    }
}
