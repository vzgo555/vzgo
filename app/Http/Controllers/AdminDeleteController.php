<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdminDeleteController extends Controller
{
    public function index(){
        $id = $_POST['user_id'];
        $cv = DB::table('users')->where('id', $id)->get()[0];
        if($cv->cv != ''){
            unlink(public_path('Uploads/'.explode('@', $cv->email)[0].'/'.$cv->cv));
            rmdir('Uploads/'.explode('@', $cv->email)[0]);
        }
        $delete = DB::table('users')->where('id', $id)->delete();
        if($delete){
            echo 1;
        }else{
            echo 0;
        }
    }

}
