<?php

namespace App\Http\Controllers;

use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AdminEditController extends Controller
{
    public function index(Request $request,$id){
        $data = DB::table('users')->where('id', $id)->get();
        if($request->isMethod('post')){
            $this->validate($request, [
                'email' => 'required|string|email|unique:users,email,'.$id,
            ]);
            $user = $request->all();
            $dir_old_name = explode('@', $data['0']->email)[0];
            $dir_new_name = explode('@', $user['email'])[0];
            unset($user['_token']);
            unset($user['admin_edit']);
            unset($user['resume']);
            $cv = $request->resume;
            if($cv != ''){
                $this->validate($request, [
                    'resume' => 'required|file|mimes:doc,pdf,txt',
                ]);
                if ($request->hasFile('resume')) {
                    $file = $request->file('resume');
                    $filename = $file->getClientOriginalName();
                    $destinationPath = public_path('/Uploads/'.$dir_old_name);
                    $file->move($destinationPath, $filename);
                }
                $user['cv'] = $filename;
            }
            if($dir_old_name != $dir_new_name){
                rename(public_path('Uploads/'.$dir_old_name), public_path('Uploads/'.$dir_new_name));
//                File::move(resource_path('views/projects/'.$oldSlug.'.blade.php'),resource_path('views/projects/'.$project->slug.'.blade.php'));
            }
            $update = DB::table('users')->where('id', $id)->update($user);
            if($update){
                return redirect()->action('AdminController@index');
            }

        }
        return view('adminedit')->with(compact('data'));
    }


}
