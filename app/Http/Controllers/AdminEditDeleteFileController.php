<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminEditDeleteFileController extends Controller
{
    public function index(){
        $user_id = $_POST['user_id'];
        $cv = DB::table('users')->where('id', $user_id)->get()[0];
        if($cv->cv != ''){
            unlink(public_path('Uploads/'.explode('@', $cv->email)[0].'/'.$cv->cv));
        }
        $user['cv'] = '';
        $update = DB::table('users')->where('id', $user_id)->update($user);
        if($update){
            echo 1;
        }else{
            echo 0;
        }
    }
}
