@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Firstname</th>
                <th scope="col">Lastname</th>
                <th scope="col">Email</th>
                <th scope="col">Birthday</th>
                <th scope="col">Gender</th>
                <th scope="col">CV</th>
                <th scope="col">Role</th>
                <th scope="col">#</th>
                <th scope="col">#</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $user)
                <tr>
                        <td>{{ $user->id }}</td>
                    <td>{{ $user->firstname }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->bday }}</td>
                    <td>{{ $user->gender }}</td>
                    <td>{{ $user->cv }}</td>
                    <td>{{ $user->role }}</td>
                    <td><a href="{{route('adminedit',['id'=>$user->id])}}"><button type="button" class="btn btn-warning">Edit</button></a></td>
                    <td><button type="button" class="btn btn-danger admin_delete" data-id="{{ $user->id }}">Delete</button></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection




