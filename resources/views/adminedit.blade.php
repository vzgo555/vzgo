@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit user options') }}
                        <a href="{{ route('admin') }}" style="float:right">
                            <button type="button" class="btn back_admin">
                                {{ __('Back to admin') }}
                            </button>
                        </a>
                    </div>

                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('FirstName') }}</label>

                                <div class="col-md-6">
                                    <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ $data[0]->firstname }}" required autofocus>

                                    @if ($errors->has('firstname'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('LastName') }}</label>

                                <div class="col-md-6">
                                    <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ $data[0]->lastname }}" required autofocus>

                                    @if ($errors->has('lastname'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $data[0]->email }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bday" class="col-md-4 col-form-label text-md-right">{{ __('BirthDay') }}</label>

                                <div class="col-md-6">
                                    <input id="bday" type="date" class="form-control{{ $errors->has('bday') ? ' is-invalid' : '' }}" name="bday" value="{{ $data[0]->bday }}" required autofocus>

                                    @if ($errors->has('bday'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bday') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                                <div class="col-md-6" style="margin-top:8px">
                                    <label class="radio-inline"><input type="radio" name="gender" value="male" @if($data[0]->gender == 'male') checked @endif>Male</label>
                                    <label style="margin-left:5px" class="radio-inline"><input type="radio" name="gender" value="female" @if($data[0]->gender == 'female') checked @endif>Female</label>
                                </div>
                            </div>

                            @if($data[0]->cv != '')
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{ __('File') }}</label>

                                    <div class="col-md-6" style="margin-top:8px">
                                        <div class="file">
                                            <a href="{{ '/files/'.$data[0]->cv }}" download>{{ $data[0]->firstname }}`s CV</a>
                                            <span data-id="{{ $data[0]->id }}"></span>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if($data[0]->cv == '')
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{ __('Add file') }}</label>

                                    <div class="col-md-6" style="margin-top:8px">
                                        <input id="resume" type="file" class="form-control {{ $errors->has('resume') ? ' is-invalid' : '' }}" style="border:0px;padding:0;" name="resume">

                                        @if ($errors->has('resume') == 1)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('resume') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            @endif


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" name="admin_edit">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection