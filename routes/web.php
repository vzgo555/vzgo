<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin');
Route::post('/admindelete', 'AdminDeleteController@index')->name('admindelete');
Route::any('/adminedit/{id}', 'AdminEditController@index')->name('adminedit');
Route::post('/admineditdeletefile', 'AdminEditDeleteFileController@index')->name('admineditdeletefile');
